import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;

import com.test.memegen.R;
import com.test.memegen.activities.MainActivity;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onData;
import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.core.IsAnything.anything;

@RunWith(AndroidJUnit4.class)
@LargeTest
public class MainActivityTests{


    private String testString;

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void initValidString() {
        // Specify a valid string.
        testString = "test";
    }

    @Test
    public void mainActivitySimpleTest() {
        onView(withId(R.id.main_activity_title))
                .check(matches(withText(testString)));
    }

    @Test
    public void mainActivityAddMemeTest() {
        onView(withId(R.id.add_mem)).perform(click());
        onView(withId(R.id.content_layout)).check(matches(isDisplayed()));
    }

    @Test
    public void mainActivityAddBackgroundTest() {
        onView(withId(R.id.add_mem)).perform(click());
        onView(withId(R.id.add_background)).perform(click());
        onView(withId(R.id.gallery_layout)).check(matches(isDisplayed()));
    }

    @Test
    public void mainActivityDefaultBackgroundPickTest() {
        onView(withId(R.id.add_mem)).perform(click());
        onView(withId(R.id.add_background)).perform(click());
        onView(withId(R.id.default_layout)).perform(click());
        onView(withId(R.id.background_list)).check(matches(isDisplayed()));
    }

    @Test
    public void mainActivityPickDefaultBackgroundTest() {
        onView(withId(R.id.add_mem)).perform(click());
        onView(withId(R.id.add_background)).perform(click());
        onView(withId(R.id.default_layout)).perform(click());
        onData(anything()).inAdapterView(withId(R.id.background_list)).atPosition(0).perform(click());
        onView(withId(R.id.content_layout)).check(matches(isDisplayed()));
    }

    @Test
    public void saveMemeFlowTest() {
        onView(withId(R.id.add_mem)).perform(click());
        onView(withId(R.id.add_background)).perform(click());
        onView(withId(R.id.default_layout)).perform(click());
        onData(anything()).inAdapterView(withId(R.id.background_list)).atPosition(0).perform(click());
        onView(withId(R.id.save_meme)).perform(click());
        onView(withId(R.id.add_mem)).check(matches(isDisplayed()));
    }


    @Test
    public void addAndDeleteTest() {
        onView(withId(R.id.add_mem)).perform(click());
        onView(withId(R.id.add_background)).perform(click());
        onView(withId(R.id.default_layout)).perform(click());
        onData(anything()).inAdapterView(withId(R.id.background_list)).atPosition(0).perform(click());
        onView(withId(R.id.save_meme)).perform(click());
        onData(anything()).inAdapterView(withId(R.id.memes_list)).atPosition(0).onChildView(withId(R.id.remove_meme)).perform(click());
        onView(withId(R.id.main_title)).check(matches(isDisplayed()));
    }


}
