package com.test.memegen.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.ColorInt;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;

import com.test.memegen.R;
import com.test.memegen.events.BackgroundSelectedEvent;
import com.test.memegen.events.ColorPickedEvent;
import com.test.memegen.events.RemoveMemeEvent;
import com.test.memegen.events.SaveBitmapEvent;
import com.test.memegen.events.ShareImgurEvent;
import com.test.memegen.events.ShareMemeEvent;
import com.test.memegen.events.ShowScreenEvent;
import com.test.memegen.events.UpdateListEvent;
import com.test.memegen.fragments.BackgroundSourceDialogFragment;
import com.test.memegen.fragments.CreateMemeFragment;
import com.test.memegen.fragments.ImageListDialogFragment;
import com.test.memegen.fragments.MainFragment;
import com.test.memegen.fragments.ShowMemeDialogFragment;
import com.test.memegen.utils.BitmapTools;
import com.test.memegen.utils.FileManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.io.IOException;
import java.util.List;

import me.priyesh.chroma.ChromaDialog;
import me.priyesh.chroma.ColorSelectListener;

public class MainActivity extends BaseActivity {

    private static final int TAKE_PICTURE = 1234;
    private static final int PICK_IMAGE = 1235;
    FileManager fileManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportFragmentManager().beginTransaction().replace(R.id.container, MainFragment.newInstance()).commit();
        fileManager = new FileManager(this);
    }

    Bitmap savedPicture;
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case TAKE_PICTURE:
                if(resultCode == RESULT_OK) {
                    Bundle extras = data.getExtras();
                    savedPicture = BitmapTools.scaleDown((Bitmap) extras.get("data"), 250, true);
                }
            break;
            case PICK_IMAGE:
                if(resultCode == RESULT_OK && data != null) {
                    Uri uri = data.getData();
                    try {
                        savedPicture = BitmapTools.scaleDown(MediaStore.Images.Media.getBitmap(getContentResolver(), uri), 450, true);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            break;
        }
    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
        if(savedPicture != null){
            EventBus.getDefault().post(new BackgroundSelectedEvent(savedPicture));
            savedPicture = null;
        }
    }

    private void takePhoto() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            startActivityForResult(takePictureIntent, TAKE_PICTURE);
        }
    }

    private void pickPhotoFromGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE);
    }

    @Subscribe
    public void onEvent(ShowScreenEvent event){
        switch (event.getScreen()){
            case CREATE_MEME:
                getSupportFragmentManager().beginTransaction().replace(R.id.container,CreateMemeFragment.newInstance()).addToBackStack(null).commit();
                break;
            case PICK_BACKGROUND:
                BackgroundSourceDialogFragment backgroundSourceDialogFragment = new BackgroundSourceDialogFragment();
                backgroundSourceDialogFragment.show(getSupportFragmentManager(),"BackgroundSourceDialogFragment");
                break;
            case DEFAULT_BACKGROUND:
                ImageListDialogFragment dialogFragment = ImageListDialogFragment.newInstance(event.isImgur());
                dialogFragment.show(getSupportFragmentManager(),"ImageListDialog");
                break;
            case GALLERY_BACKGROUND:
                pickPhotoFromGallery();
                break;
            case CAMERA_BACKGROUND:
                takePhoto();
                break;
            case PICK_COLOR:
                new ChromaDialog.Builder().initialColor(event.getColor()).onColorSelected(new ColorSelectListener() {
                    @Override
                    public void onColorSelected(@ColorInt int color) {
                        EventBus.getDefault().post(new ColorPickedEvent(color));
                    }
                }).create().show(getSupportFragmentManager(),"ChromaDialog");
                break;
            case MAIN:
                getSupportFragmentManager().popBackStack();
                break;
            case SHOW_MEME:
                ShowMemeDialogFragment showMemeDialogFragment = ShowMemeDialogFragment.newInstance(event.getBitmap());
                showMemeDialogFragment.show(getSupportFragmentManager(),"ShowMemeDialogFragment");
                break;
            default:
                throw new IllegalStateException("Unsupported screen change: " + event.getScreen().toString());
        }
    }

    @Subscribe
    public void onEvent(SaveBitmapEvent event){
        final Bitmap bitmap = event.getBitmap();
        new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                fileManager.saveBitmap(bitmap);
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                EventBus.getDefault().post(new UpdateListEvent());
            }
        }.execute();
    }

    @Subscribe
    public void onEvent(ShareImgurEvent event){
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("text/plain");
        intent.putExtra(android.content.Intent.EXTRA_SUBJECT, "Checkout my new meme");
        intent.putExtra(android.content.Intent.EXTRA_TEXT, event.getLink());
        startActivity(Intent.createChooser(intent, getString(R.string.share_via)));
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if (fragment != null) {
                    fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
                }
            }
        }
    }

    @Subscribe
    public void onEvent(ShareMemeEvent event){
        String path = MediaStore.Images.Media.insertImage(getContentResolver(),event.getMeme().getBitmap(),"",null);
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("image/jpeg");
        intent.putExtra(Intent.EXTRA_STREAM, Uri.parse(path));
        startActivity(Intent.createChooser(intent, getString(R.string.share_via)));
    }

    @Subscribe
    public void onEvent(RemoveMemeEvent event){
        File file = new File(event.getMeme().getPath());
        file.delete();
        EventBus.getDefault().post(new UpdateListEvent());
    }
}
