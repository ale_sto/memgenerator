package com.test.memegen.network;


import com.test.memegen.responses.AlbumResponse;
import com.test.memegen.responses.ImageResponse;

import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.GET;
import retrofit.http.Header;
import retrofit.http.POST;
import retrofit.mime.TypedFile;


public interface ImgurService {

    /****************************************
     * Upload
     * Image upload API
     */

    /**
     * @param auth        #Type of authorization for upload
     * @param file        image
     */
    @POST("/3/image")
    void postImage(
            @Header("Authorization") String auth,
            @Body TypedFile file,
            Callback<ImageResponse> cb
    );

    @GET("/3/album/oZVFe")
    void getAlbums(
            @Header("Authorization") String auth,
            Callback<AlbumResponse> cb);
}
