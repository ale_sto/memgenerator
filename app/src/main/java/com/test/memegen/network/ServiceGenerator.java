package com.test.memegen.network;

import com.squareup.okhttp.OkHttpClient;

import retrofit.RestAdapter;
import retrofit.client.OkClient;

public class ServiceGenerator {

    public static String IMGUR_BASE_URL = "https://api.imgur.com";

    private static RestAdapter.Builder builder =
            new RestAdapter.Builder()
                    .setClient(new OkClient(new OkHttpClient()));


    public static <S> S createService(Class<S> serviceClass) {
        builder.setEndpoint(IMGUR_BASE_URL);

        RestAdapter restAdapter = builder.build();
        restAdapter.setLogLevel(RestAdapter.LogLevel.FULL);
//        if(BuildConfig.DEBUG) {
//        }
        return restAdapter.create(serviceClass);
    }
}
