package com.test.memegen.models;

import android.graphics.Bitmap;

public class Meme {
    Bitmap bitmap;
    String path;

    public Meme(String path) {
        this.path = path;
    }

    public Meme(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public Meme(Bitmap bitmap, String path) {
        this.bitmap = bitmap;
        this.path = path;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
