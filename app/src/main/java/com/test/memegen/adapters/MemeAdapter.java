package com.test.memegen.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;

import com.test.memegen.R;
import com.test.memegen.events.RemoveMemeEvent;
import com.test.memegen.events.SendMemeEvent;
import com.test.memegen.events.ShareMemeEvent;
import com.test.memegen.models.Meme;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

public class MemeAdapter extends ArrayAdapter<Meme> {
    private static class ViewHolder {
        ImageView image;
        Button share;
        Button remove;
        Button send;
    }

    public MemeAdapter(Context context, List<Meme> objects) {
        super(context, 0, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Meme meme = getItem(position);
        ViewHolder viewHolder;
        if(convertView == null){
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.meme_list_item,parent,false);
            viewHolder.image = (ImageView) convertView.findViewById(R.id.meme_image);
            viewHolder.share = (Button) convertView.findViewById(R.id.share_meme);
            viewHolder.remove = (Button) convertView.findViewById(R.id.remove_meme);
            viewHolder.send = (Button) convertView.findViewById(R.id.imgur_meme);
            viewHolder.send.setOnClickListener(sendOnClickListener);
            viewHolder.share.setOnClickListener(shareOnClickListener);
            viewHolder.remove.setOnClickListener(removeOnClickListener);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.image.setImageBitmap(meme.getBitmap());
        viewHolder.share.setTag(position);
        viewHolder.remove.setTag(position);
        viewHolder.send.setTag(position);
        return convertView;
    }

    @Override
    public void notifyDataSetChanged() {
        super.notifyDataSetChanged();
    }

    View.OnClickListener sendOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = (int)v.getTag();
            EventBus.getDefault().post(new SendMemeEvent(getItem(position)));
        }
    };

    View.OnClickListener shareOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = (int)v.getTag();
            EventBus.getDefault().post(new ShareMemeEvent(getItem(position)));
        }
    };

    View.OnClickListener removeOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            int position = (int)v.getTag();
            EventBus.getDefault().post(new RemoveMemeEvent(getItem(position)));
        }
    };
}
