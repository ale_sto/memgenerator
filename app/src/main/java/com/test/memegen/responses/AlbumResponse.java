package com.test.memegen.responses;

import java.util.List;

public class AlbumResponse {

    public boolean success;
    public String status;
    public DownloadedAlbum data;

    public static class DownloadedAlbum {
        public String id;
        public String title;
        public String description;
        public int images_count;
        public List<ImageResponse.UploadedImage> images;

        @Override
        public String toString() {
            return "DownloadedAlbum{" +
                    "id='" + id + '\'' +
                    ", title='" + title + '\'' +
                    ", description='" + description + '\'' +
                    ", images_count=" + images_count +
                    ", images=" + images +
                    '}';
        }
    }

    @Override
    public String toString() {
        return "AlbumResponse{" +
                "success=" + success +
                ", status='" + status + '\'' +
                ", data=" + data +
                '}';
    }
}
