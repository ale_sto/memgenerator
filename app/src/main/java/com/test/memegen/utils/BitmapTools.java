package com.test.memegen.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;

public class BitmapTools {

    Context context;

    public BitmapTools(Context context) {
        this.context = context;
    }

    public static Bitmap scaleDown(Bitmap realImage, float maxImageSize,
                            boolean filter) {
        float ratio = Math.min(
                (float) maxImageSize / realImage.getWidth(),
                (float) maxImageSize / realImage.getHeight());
        int width = Math.round((float) ratio * realImage.getWidth());
        int height = Math.round((float) ratio * realImage.getHeight());

        Bitmap newBitmap = Bitmap.createScaledBitmap(realImage, width,
                height, filter);
        return newBitmap;
    }

    public Bitmap drawTextsOnMeme(Bitmap source, String topText, String bottomText){
        Bitmap top = drawMultilineTextToBitmap(source,topText,true ,Color.WHITE, Const.TEXT_PADDING);
        return drawMultilineTextToBitmap(top, bottomText, false, Color.WHITE, Const.TEXT_PADDING);
    }

    private Bitmap drawMultilineTextToBitmap(Bitmap source, String text, boolean top, int color, int padding){
        if(source == null || text == null){
            return null;
        }
        Bitmap copy = source.copy(Bitmap.Config.ARGB_8888, true);
        float scale = context.getResources().getDisplayMetrics().density;
        Canvas canvas = new Canvas(copy);
        Typeface impactFont = Typeface.createFromAsset(context.getAssets(),"fonts/impact.ttf");
        TextPaint textPaint = new TextPaint(Paint.ANTI_ALIAS_FLAG);
        textPaint.setColor(color);
        int height = getFontSizePx(source);
        textPaint.setTextSize(height);
        textPaint.setTypeface(impactFont);
        textPaint.setShadowLayer(10.0f,0.0f,0.0f, Color.BLACK);

        int textWidth = canvas.getWidth() - (int)(padding * scale);
        StaticLayout staticLayout = new StaticLayout(text,textPaint,textWidth, Layout.Alignment.ALIGN_CENTER,1.0f,0.0f,false);
        int textHeight = staticLayout.getHeight();

        float x = (copy.getWidth() - textWidth)/2;
        float y = top ? padding : copy.getHeight() - textHeight - padding;

        canvas.save();
        canvas.translate(x, y);
        staticLayout.draw(canvas);
        canvas.restore();

        return copy;
    }

    public int getFontSizePx(Bitmap bitmap){
        return bitmap.getHeight() / 10;
    }
}

