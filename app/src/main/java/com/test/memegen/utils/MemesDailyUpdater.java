package com.test.memegen.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.Date;
import java.util.List;

public class MemesDailyUpdater {

    Context context;

    public MemesDailyUpdater(Context context) {
        this.context = context;
    }

    public List<String> loadMemeUrls(){
        SharedPreferences sharedPreferences = context.getSharedPreferences(Const.SHARED_PREF,Context.MODE_PRIVATE);
        String json = sharedPreferences.getString(Const.MEMES_URLS,null);
        if(json == null){
            return null;
        }
        Gson gson = new Gson();
        return gson.fromJson(json,new TypeToken<List<String>>(){}.getType());
    }

    public boolean shouldUpdate(){
        SharedPreferences sharedPreferences = context.getSharedPreferences(Const.SHARED_PREF,Context.MODE_PRIVATE);
        String json = sharedPreferences.getString(Const.MEMES_URLS,null);
        if(json == null){
            return true;
        }
        long millis = sharedPreferences.getLong(Const.MEMES_UPDATE_DATE,10000000);
        Date now = new Date();
        return now.getTime() - millis > 24 * 60 * 60 * 1000;

    }

    public void saveMemes(List<String> list, long dateMillis){
        SharedPreferences sharedPreferences = context.getSharedPreferences(Const.SHARED_PREF,Context.MODE_PRIVATE);
        Gson gson = new Gson();
        sharedPreferences.edit().putString(Const.MEMES_URLS, gson.toJson(list))
                .putLong(Const.MEMES_UPDATE_DATE, dateMillis)
                .apply();
    }
}
