package com.test.memegen.utils;

public class Const {

//    Text parameters
    public static int MAX_TEXT_LENGTH = 50;
    public static int TEXT_PADDING = 10;

//    Files parameters
    public static String SHARED_PREF = "meme_generator_sp";
    public static String FILE_PATH = "/MemeGenerator";
    public static String MEME_COUNTER = "meme_counter";
    public static String BASE_NAME = "meme_";
    public static String EXTENSION = ".jpeg";

//    Splash screen
    public static int SPLASH_MILLIS = 1200;

//    MemesDaily Updater
    public static String MEMES_URLS = "memes_urls";
    public static String MEMES_UPDATE_DATE = "memes_update_date";

//    Imgur API Const
    public static String IMGUR_CLIENT_ID = "c6b250cfaf73f1d";
    public static String IMGUR_CLIENT_SECRET = "26fd25c3cecd83f119c7dd5d1d78a6c64ec39aa1";


    public static String getClientAuth() {
        return "Client-ID " + IMGUR_CLIENT_ID;
    }
}
