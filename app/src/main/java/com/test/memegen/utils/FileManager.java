package com.test.memegen.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

import com.test.memegen.models.Meme;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileManager {

    Context context;

    public FileManager(Context context) {
        this.context = context;
        createDirectoryIfNeeded();
    }

    private void createDirectoryIfNeeded(){
        File path = new File(Environment.getExternalStorageDirectory().getAbsolutePath() + Const.FILE_PATH);
        path.mkdirs();
    }

    public void saveBitmap(Bitmap bitmap) {
        SharedPreferences sharedPreferences = context.getSharedPreferences(Const.SHARED_PREF,Context.MODE_PRIVATE);
        int number = sharedPreferences.getInt(Const.MEME_COUNTER,0) + 1;
        StringBuilder builder = new StringBuilder(Environment.getExternalStorageDirectory().getAbsolutePath());
        builder.append(Const.FILE_PATH).append("/").append(Const.BASE_NAME).append(number).append(Const.EXTENSION);
        String path =  builder.toString();
        FileOutputStream fileOutputStream = null;
        try{
            fileOutputStream = new FileOutputStream(path);
            bitmap.compress(Bitmap.CompressFormat.JPEG,30,fileOutputStream);
            sharedPreferences.edit().putInt(Const.MEME_COUNTER,number).commit();
        }  catch (FileNotFoundException e) {
            e.printStackTrace();
        } finally {
            if(fileOutputStream != null){
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public List<Meme> loadMemes(){
        List<Meme> list = new ArrayList<>();
        File dir = new File(Environment.getExternalStorageDirectory().toString() + Const.FILE_PATH);
        File[] files = dir.listFiles();
        if(files !=null){
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.ARGB_8888;
            for(int i = files.length - 1; i >= 0; i--){
                list.add(new Meme(BitmapFactory.decodeFile(files[i].getPath(),options),files[i].getPath()));
            }
        }
        return list;
    }
}
