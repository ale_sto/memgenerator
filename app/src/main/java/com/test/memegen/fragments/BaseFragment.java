package com.test.memegen.fragments;

import android.support.v4.app.Fragment;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;

public abstract class BaseFragment extends Fragment {

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        EventBus.getDefault().unregister(this);
        super.onStop();
    }

    protected void showToast(String text) {
        Toast.makeText(getActivity(),text,Toast.LENGTH_SHORT).show();
    }


}
