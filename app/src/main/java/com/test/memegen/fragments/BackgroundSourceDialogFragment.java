package com.test.memegen.fragments;


import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.test.memegen.R;
import com.test.memegen.enums.Screen;
import com.test.memegen.events.ShowScreenEvent;

import org.greenrobot.eventbus.EventBus;

import butterknife.ButterKnife;
import butterknife.OnClick;

public class BackgroundSourceDialogFragment extends DialogFragment {



    public BackgroundSourceDialogFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.background_source_dialog_fragment, container, false);
        ButterKnife.bind(this,view);

        return view;
    }

    @OnClick(R.id.gallery_layout)
    public void onGalleryClick(View view) {
        changeScreen(Screen.GALLERY_BACKGROUND, false);
    }

    @OnClick(R.id.camera_layout)
    public void onCameraClick(View view) {
        changeScreen(Screen.CAMERA_BACKGROUND, false);
    }

    @OnClick(R.id.default_layout)
    public void onDefaultClick(View view) {
        changeScreen(Screen.DEFAULT_BACKGROUND, false);
    }

    @OnClick(R.id.imgur_download_layout)
    public void onImgurDownloadClick(View view) {
        changeScreen(Screen.DEFAULT_BACKGROUND, true);
    }

    private void changeScreen(Screen screen, boolean imgurDownload) {
        dismiss();
        EventBus.getDefault().post(new ShowScreenEvent(screen, imgurDownload));
    }
}
