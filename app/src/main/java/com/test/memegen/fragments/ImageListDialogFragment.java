package com.test.memegen.fragments;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.test.memegen.R;
import com.test.memegen.adapters.BackgroundAdapter;
import com.test.memegen.events.BackgroundSelectedEvent;
import com.test.memegen.network.ImgurService;
import com.test.memegen.network.ServiceGenerator;
import com.test.memegen.responses.AlbumResponse;
import com.test.memegen.responses.ImageResponse;
import com.test.memegen.utils.Const;
import com.test.memegen.utils.MemesDailyUpdater;
import com.test.memegen.utils.NetworkUtils;

import org.greenrobot.eventbus.EventBus;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class ImageListDialogFragment extends DialogFragment implements AdapterView.OnItemClickListener{
    private static final String IMGUR_DOWNLAOD = "imgur_downlaod";

    List<Bitmap> bitmapList = new ArrayList<>();
    List<String> imagesUrlList;
    BackgroundAdapter adapter;
    MemesDailyUpdater memesDailyUpdater;
    boolean imgurDownload;

    @Bind(R.id.background_list)
    ListView backgroundsList;

    @Bind(R.id.progress_bar)
    ProgressBar progress;

    public static ImageListDialogFragment newInstance(boolean imgurDownload){
        ImageListDialogFragment fragment = new ImageListDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(IMGUR_DOWNLAOD,imgurDownload);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments() != null){
            imgurDownload = getArguments().getBoolean(IMGUR_DOWNLAOD);
        } else {
            imgurDownload = false;
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.image_list_dialog_fragment, null, false);
        ButterKnife.bind(this, view);
        adapter = new BackgroundAdapter(getActivity(),bitmapList);
        backgroundsList.setAdapter(adapter);
        backgroundsList.setOnItemClickListener(this);
        memesDailyUpdater = new MemesDailyUpdater(getContext());
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        if(imgurDownload) {
            if (NetworkUtils.isConnected(getContext())) {
                if (memesDailyUpdater.shouldUpdate()) {
                    downloadMemes();
                } else {
                    imagesUrlList = memesDailyUpdater.loadMemeUrls();
                    showLoading();
                    getBitmapsAsync();
                }
            } else {
                getBitmapList();
            }
        } else {
            getBitmapList();
        }

        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        dismiss();
        EventBus.getDefault().post(new BackgroundSelectedEvent(bitmapList.get(position)));
    }

    private void downloadMemes() {
        showLoading();
        ImgurService service = ServiceGenerator.createService(ImgurService.class);
        service.getAlbums(Const.getClientAuth(), new Callback<AlbumResponse>() {
            @Override
            public void success(AlbumResponse albumResponse, Response response) {
                if(albumResponse != null && albumResponse.success){
                    if(albumResponse.data.images_count > 0){
                        imagesUrlList = new ArrayList<String>();
                        Date now = new Date();
                        for(ImageResponse.UploadedImage image : albumResponse.data.images) {
                            imagesUrlList.add(image.link);
                        }
                        memesDailyUpdater.saveMemes(imagesUrlList,now.getTime());
                        getBitmapsAsync();
                    } else {
                        getBitmapList();
                    }
                }
            }

            @Override
            public void failure(RetrofitError error) {
                getBitmapList();
            }
        });
    }

    private void getBitmapsAsync(){
        Toast.makeText(getContext(),getString(R.string.downloading_memes),Toast.LENGTH_SHORT).show();
        AsyncTask<Void, Void, Void> task = new AsyncTask<Void, Void, Void>(){

            @Override
            protected Void doInBackground(Void... params) {
                bitmapList.clear();
                for(String url : imagesUrlList){
                    try {
                        Bitmap bitmap = BitmapFactory.decodeStream((InputStream)new URL(url).getContent());
                        bitmapList.add(bitmap);
                    } catch (MalformedURLException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);
                adapter.notifyDataSetChanged();
                disableLoading();
            }
        };
        task.execute();
    }

    public void getBitmapList() {
        bitmapList.clear();
        bitmapList.add(BitmapFactory.decodeResource(getResources(),R.drawable.meme1));
        bitmapList.add(BitmapFactory.decodeResource(getResources(),R.drawable.meme2));
        bitmapList.add(BitmapFactory.decodeResource(getResources(),R.drawable.meme3));
        bitmapList.add(BitmapFactory.decodeResource(getResources(),R.drawable.meme4));
        bitmapList.add(BitmapFactory.decodeResource(getResources(),R.drawable.meme5));
        bitmapList.add(BitmapFactory.decodeResource(getResources(),R.drawable.meme6));
        bitmapList.add(BitmapFactory.decodeResource(getResources(),R.drawable.meme7));
        adapter.notifyDataSetChanged();
        disableLoading();
    }

    private void showLoading() {
        backgroundsList.setVisibility(View.GONE);
        progress.setVisibility(View.VISIBLE);
    }

    private void disableLoading() {
        backgroundsList.setVisibility(View.VISIBLE);
        progress.setVisibility(View.GONE);
    }
}
