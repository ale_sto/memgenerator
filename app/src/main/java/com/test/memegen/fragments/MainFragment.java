package com.test.memegen.fragments;


import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.test.memegen.R;
import com.test.memegen.adapters.MemeAdapter;
import com.test.memegen.enums.Screen;
import com.test.memegen.events.SendMemeEvent;
import com.test.memegen.events.ShareImgurEvent;
import com.test.memegen.events.ShowScreenEvent;
import com.test.memegen.events.UpdateListEvent;
import com.test.memegen.models.Meme;
import com.test.memegen.network.ImgurService;
import com.test.memegen.network.ServiceGenerator;
import com.test.memegen.responses.ImageResponse;
import com.test.memegen.utils.Const;
import com.test.memegen.utils.FileManager;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnItemClick;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.mime.TypedFile;

public class MainFragment extends BaseFragment {
    public static final String TAG = MainFragment.class.getSimpleName();

    ArrayList<Meme> memeList = new ArrayList<>();
    FileManager fileManager;
    MemeAdapter memeAdapter;

    @Bind(R.id.progress_bar)
    ProgressBar progressBar;

    @Bind(R.id.no_memes)
    TextView addMemes;

    @Bind(R.id.memes_list)
    ListView listView;

    public MainFragment() {
    }
    public static MainFragment newInstance() {
        MainFragment fragment = new MainFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_main, container, false);
        ButterKnife.bind(this,view);
        fileManager = new FileManager(getActivity());
        memeAdapter = new MemeAdapter(getActivity(),memeList);
        listView.setAdapter(memeAdapter);
        loadMemesAsync();
        return view;
    }

    private void loadMemesAsync() {
        showLoading();
        new AsyncTask<Void, Void, List<Meme>>() {
            @Override
            protected List<Meme> doInBackground(Void... params) {
                return fileManager.loadMemes();
            }

            @Override
            protected void onPostExecute(List<Meme> list) {
                super.onPostExecute(list);
                memeList.clear();
                memeList.addAll(list);
                memeAdapter.notifyDataSetChanged();
                disableLoading();
            }
        }.execute();
    }

    @OnItemClick(R.id.memes_list)
    public void onMemeClick(int position) {
        EventBus.getDefault().post(new ShowScreenEvent(Screen.SHOW_MEME, memeList.get(position).getBitmap()));
    }

    @OnClick(R.id.add_mem)
    public void onAddMemeClick(View view){
        EventBus.getDefault().post(new ShowScreenEvent(Screen.CREATE_MEME));
    }

    @Subscribe
    public void onEvent(UpdateListEvent event){
        loadMemesAsync();
    }

    @Subscribe
    public void onEvent(SendMemeEvent event){
        showLoading();
        ImgurService service = ServiceGenerator.createService(ImgurService.class);
        File file = new File(event.getMeme().getPath());
        service.postImage(Const.getClientAuth(), new TypedFile("image/*", file), new Callback<ImageResponse>() {
            @Override
            public void success(ImageResponse imageResponse, Response response) {
                if(imageResponse != null && imageResponse.success){
                    Log.d(TAG,"Image sent! Link: " + imageResponse.data.link);
                    EventBus.getDefault().post(new ShareImgurEvent(imageResponse.data.link));
                    disableLoading();
                } else {
                    showToast(getString(R.string.error_uploading));
                    disableLoading();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                showToast(getString(R.string.error_uploading));
                disableLoading();
            }
        });

    }

    private void showLoading(){
        progressBar.setVisibility(View.VISIBLE);
        listView.setVisibility(View.GONE);
    }

    private void disableLoading(){
        progressBar.setVisibility(View.GONE);
        if(memeList.size() == 0){
            addMemes.setVisibility(View.VISIBLE);
            listView.setVisibility(View.GONE);
        } else {
            addMemes.setVisibility(View.GONE);
            listView.setVisibility(View.VISIBLE);
        }
    }
}
