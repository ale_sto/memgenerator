package com.test.memegen.fragments;


import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.ScrollView;
import android.widget.TextView;

import com.test.memegen.R;
import com.test.memegen.enums.Screen;
import com.test.memegen.events.BackgroundSelectedEvent;
import com.test.memegen.events.ColorPickedEvent;
import com.test.memegen.events.SaveBitmapEvent;
import com.test.memegen.events.ShowScreenEvent;
import com.test.memegen.utils.BitmapTools;
import com.test.memegen.utils.Const;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CreateMemeFragment extends BaseFragment {
    public static final String TAG = CreateMemeFragment.class.getSimpleName();
    private static final int MY_PERMISSIONS_REQUEST_EXTERNAL_STORAGE = 5;

    public CreateMemeFragment() {

    }
    public static CreateMemeFragment newInstance() {
        CreateMemeFragment fragment = new CreateMemeFragment();
        return fragment;
    }

    int currentColor;
    Bitmap currentBitmap;
    Bitmap tempBitmap;
    String topText;
    String bottomText;
    BitmapTools bitmapTools;

    @Bind(R.id.meme_background)
    ImageView tempBackground;

    @Bind(R.id.meme_temp)
    ImageView background;

    @Bind(R.id.text_color)
    ImageView textColor;

    @Bind(R.id.content_layout)
    ScrollView content;

    @Bind(R.id.no_permission)
    TextView noPermission;

    @Bind(R.id.add_text)
    EditText addText;

    @Bind(R.id.bottom_radio)
    RadioButton bottomRadio;

    @Bind(R.id.top_radio)
    RadioButton topRadio;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_create_meme, container, false);
        ButterKnife.bind(this,view);
        addText.addTextChangedListener(textWatcher);
        currentColor = addText.getCurrentTextColor();
        bitmapTools = new BitmapTools(getActivity());
        topText = new String();
        bottomText = new String();

        if (ContextCompat.checkSelfPermission(getContext(),
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            disableContent();

            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                    MY_PERMISSIONS_REQUEST_EXTERNAL_STORAGE);

        }
        topRadio.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked) {
                    addText.setText(topText);
                } else {
                    addText.setText(bottomText);
                }
            }
        });
        return view;
    }

    @OnClick(R.id.text_color)
    public void onTextColorClick(View view){
        EventBus.getDefault().post(new ShowScreenEvent(Screen.PICK_COLOR,currentColor));
    }

    @OnClick(R.id.add_background)
    public void onAddBackgroundClick(View view){
        EventBus.getDefault().post(new ShowScreenEvent(Screen.PICK_BACKGROUND));
    }

    @OnClick(R.id.save_meme)
    public void onSaveButtonClick(View view){
        if(currentBitmap == null){
            showToast(getString(R.string.add_background_first));
            return;
        }
        EventBus.getDefault().post(new SaveBitmapEvent(tempBitmap));
        EventBus.getDefault().post(new ShowScreenEvent(Screen.MAIN));
    }


    @Subscribe
    public void onEvent(BackgroundSelectedEvent event){
        currentBitmap = event.getBackground();
        tempBitmap = Bitmap.createBitmap(currentBitmap);
        drawTextsOnBitmap();
    }

    @Subscribe
    public void onEvent(ColorPickedEvent event){
        addText.setTextColor(event.getColor());
        textColor.setBackgroundColor(event.getColor());
        currentColor = event.getColor();
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {

        }

        @Override
        public void afterTextChanged(Editable s) {
            String text = addText.getText().toString();
            if(text.length() > Const.MAX_TEXT_LENGTH){
                addText.setError(getString(R.string.error_hint_long) + " " +  Const.MAX_TEXT_LENGTH);
            } else {
                addText.setError(null);
            }
            if(topRadio.isChecked()){
                topText = text;
            } else {
                bottomText = text;
            }
            drawTextsOnBitmap();
        }
    };

    private void drawTextsOnBitmap(){
        if(currentBitmap == null){
            return;
        }
        tempBitmap = bitmapTools.drawTextsOnMeme(currentBitmap,topText,bottomText);
        background.setImageBitmap(tempBitmap);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_EXTERNAL_STORAGE: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    showContent();
                } else {
                    showToast(getString(R.string.external_storage_permission));
                    disableContent();
                }
                return;
            }
        }
    }

    private void showContent(){
        content.setVisibility(View.VISIBLE);
        noPermission.setVisibility(View.GONE);
    }

    private void disableContent(){
        content.setVisibility(View.GONE);
        noPermission.setVisibility(View.VISIBLE);
    }
}
