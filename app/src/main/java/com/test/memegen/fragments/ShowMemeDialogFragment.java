package com.test.memegen.fragments;


import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.test.memegen.R;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ShowMemeDialogFragment extends DialogFragment {
    private static final String MEME = "meme_imge";

    Bitmap memeBitmap;

    @Bind(R.id.meme_image)
    ImageView memeImageView;

    public ShowMemeDialogFragment() {
    }

    public static ShowMemeDialogFragment newInstance(Bitmap memeBitmap) {
        ShowMemeDialogFragment fragment = new ShowMemeDialogFragment();
        Bundle args = new Bundle();
        args.putParcelable(MEME,memeBitmap);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            memeBitmap = getArguments().getParcelable(MEME);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_show_meme_dialog, container, false);
        ButterKnife.bind(this,view);
        memeImageView.setImageBitmap(memeBitmap);
        return view;
    }

}
