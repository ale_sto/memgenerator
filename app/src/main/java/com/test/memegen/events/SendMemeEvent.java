package com.test.memegen.events;

import com.test.memegen.models.Meme;

public class SendMemeEvent {
    Meme meme;

    public SendMemeEvent(Meme meme) {
        this.meme = meme;
    }

    public Meme getMeme() {
        return meme;
    }

    public void setMeme(Meme meme) {
        this.meme = meme;
    }
}
