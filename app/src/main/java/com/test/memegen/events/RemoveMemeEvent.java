package com.test.memegen.events;

import com.test.memegen.models.Meme;

public class RemoveMemeEvent {
    Meme meme;

    public RemoveMemeEvent(Meme meme) {
        this.meme = meme;
    }

    public Meme getMeme() {
        return meme;
    }

    public void setMeme(Meme meme) {
        this.meme = meme;
    }
}

