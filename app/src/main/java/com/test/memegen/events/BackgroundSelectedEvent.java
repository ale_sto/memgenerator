package com.test.memegen.events;

import android.graphics.Bitmap;

public class BackgroundSelectedEvent {
    Bitmap background;

    public BackgroundSelectedEvent(Bitmap background) {
        this.background = background;
    }

    public Bitmap getBackground() {
        return background;
    }

    public void setBackground(Bitmap background) {
        this.background = background;
    }
}
