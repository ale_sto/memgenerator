package com.test.memegen.events;

import android.graphics.Bitmap;

import com.test.memegen.enums.Screen;

public class ShowScreenEvent {

    Screen screen;
    int color;
    Bitmap bitmap;
    boolean imgur;

    public ShowScreenEvent(Screen screen) {
        this.screen = screen;
    }

    public ShowScreenEvent(Screen screen, int color) {
        this.screen = screen;
        this.color = color;
    }

    public ShowScreenEvent(Screen screen, Bitmap bitmap) {
        this.screen = screen;
        this.bitmap = bitmap;
    }

    public ShowScreenEvent(Screen screen, boolean imgur) {
        this.imgur = imgur;
        this.screen = screen;
    }

    public boolean isImgur() {
        return imgur;
    }

    public void setImgur(boolean imgur) {
        this.imgur = imgur;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public Screen getScreen() {
        return screen;
    }

    public void setScreen(Screen screen) {
        this.screen = screen;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
