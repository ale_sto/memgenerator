package com.test.memegen.events;

import com.test.memegen.models.Meme;

public class ShareMemeEvent {
    Meme meme;

    public ShareMemeEvent(Meme meme) {
        this.meme = meme;
    }

    public Meme getMeme() {
        return meme;
    }

    public void setMeme(Meme meme) {
        this.meme = meme;
    }
}

