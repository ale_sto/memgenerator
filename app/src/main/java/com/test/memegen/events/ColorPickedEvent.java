package com.test.memegen.events;

public class ColorPickedEvent {
    int color;

    public ColorPickedEvent(int color) {
        this.color = color;
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }
}
