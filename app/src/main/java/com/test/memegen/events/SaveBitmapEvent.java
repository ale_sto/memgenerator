package com.test.memegen.events;

import android.graphics.Bitmap;

public class SaveBitmapEvent {

    Bitmap bitmap;

    public SaveBitmapEvent(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public Bitmap getBitmap() {
        return bitmap;
    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }
}
