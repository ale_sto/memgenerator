package com.test.memegen.events;


public class ShareImgurEvent {

    String link;

    public ShareImgurEvent(String link) {
        this.link = link;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
