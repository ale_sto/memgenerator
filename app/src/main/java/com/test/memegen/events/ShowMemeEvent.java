package com.test.memegen.events;

import android.graphics.Bitmap;

public class ShowMemeEvent {
    Bitmap meme;

    public ShowMemeEvent(Bitmap meme) {
        this.meme = meme;
    }

    public Bitmap getMeme() {
        return meme;
    }

    public void setMeme(Bitmap meme) {
        this.meme = meme;
    }
}
