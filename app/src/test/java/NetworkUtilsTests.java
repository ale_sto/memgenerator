import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import com.test.memegen.utils.NetworkUtils;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class NetworkUtilsTests {


    @Mock
    Context mockContext;

    @Mock
    ConnectivityManager mockConnectivityManager;

    @Mock
    NetworkInfo mockNetworkInfo;

    @Test
    public void connectivityManagerNullTest(){
        when(mockContext.getSystemService(Context.CONNECTIVITY_SERVICE)).thenReturn(null);
        assertFalse(NetworkUtils.isConnected(mockContext));
    }

    @Test
    public void connectivityNetworkInfoNullTest(){
        when(mockContext.getSystemService(Context.CONNECTIVITY_SERVICE)).thenReturn(mockConnectivityManager);
        when(mockConnectivityManager.getActiveNetworkInfo()).thenReturn(null);
        assertFalse(NetworkUtils.isConnected(mockContext));
    }

    @Test
    public void connectivityTrueTest(){
        when(mockContext.getSystemService(Context.CONNECTIVITY_SERVICE)).thenReturn(mockConnectivityManager);
        when(mockConnectivityManager.getActiveNetworkInfo()).thenReturn(mockNetworkInfo);
        when(mockNetworkInfo.isConnected()).thenReturn(true);
        assertTrue(NetworkUtils.isConnected(mockContext));
    }
}
