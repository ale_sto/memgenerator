import android.content.Context;
import android.content.SharedPreferences;

import com.test.memegen.utils.Const;
import com.test.memegen.utils.MemesDailyUpdater;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.Date;
import java.util.List;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MemesDailyUpdaterTests {

    @Mock
    Context mockContext;

    @Mock
    SharedPreferences mockSharedPreferences;

    @Test
    public void ShouldUpdateNoMemesTest() {
        when(mockSharedPreferences.getString(Const.MEMES_URLS,null)).thenReturn(null);
        when(mockContext.getSharedPreferences(Const.SHARED_PREF,Context.MODE_PRIVATE))
                .thenReturn(mockSharedPreferences);
        MemesDailyUpdater updater = new MemesDailyUpdater(mockContext);
        assertTrue(updater.shouldUpdate());
    }

    @Test
    public void ShouldUpdateSimpleFalseTest() {
        Date now = new Date();
        when(mockSharedPreferences.getString(Const.MEMES_URLS,null)).thenReturn(" ");
        when(mockSharedPreferences.getLong(Const.MEMES_UPDATE_DATE,10000000)).thenReturn(now.getTime() - 10000);
        when(mockContext.getSharedPreferences(Const.SHARED_PREF,Context.MODE_PRIVATE))
                .thenReturn(mockSharedPreferences);
        MemesDailyUpdater updater = new MemesDailyUpdater(mockContext);
        assertFalse(updater.shouldUpdate());
    }

    @Test
    public void ShouldUpdateSimpleTrueTest() {
        Date now = new Date();
        when(mockSharedPreferences.getString(Const.MEMES_URLS,null)).thenReturn(" ");
        when(mockSharedPreferences.getLong(Const.MEMES_UPDATE_DATE,10000000)).thenReturn(now.getTime() - 24 * 60 * 60 * 1000 - 1);
        when(mockContext.getSharedPreferences(Const.SHARED_PREF,Context.MODE_PRIVATE))
                .thenReturn(mockSharedPreferences);
        MemesDailyUpdater updater = new MemesDailyUpdater(mockContext);
        assertTrue(updater.shouldUpdate());
    }

    @Test
    public void LoadMemesNullTest() {
        when(mockSharedPreferences.getString(Const.MEMES_URLS,null)).thenReturn(null);
        when(mockContext.getSharedPreferences(Const.SHARED_PREF,Context.MODE_PRIVATE))
                .thenReturn(mockSharedPreferences);
        MemesDailyUpdater updater = new MemesDailyUpdater(mockContext);
        assertTrue(updater.loadMemeUrls() == null);
    }

    @Test
    public void LoadMemesListTest() {
        String[] test = new String[]{"test1", "test2", "test3"};
        when(mockSharedPreferences.getString(Const.MEMES_URLS,null)).thenReturn("[\"test1\", \"test2\", \"test3\"]");
        when(mockContext.getSharedPreferences(Const.SHARED_PREF,Context.MODE_PRIVATE))
                .thenReturn(mockSharedPreferences);
        MemesDailyUpdater updater = new MemesDailyUpdater(mockContext);
        List<String> list = updater.loadMemeUrls();
        for(int i = 0; i< test.length; i++) {
            assertTrue(test[i].compareTo(list.get(i)) == 0);
        }
    }
}
